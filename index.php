

<html>
<head>
 <meta charset="utf-8"/>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> 
    <link type="text/css" rel="stylesheet" href="style.css"/> 
    <script type="text/javascript" src="chat.js" ></script>  

</head>
    
<body onload="setTimeout(sendUpdate, 500)">


    <div class="navbar-wrapper">
    <div class="userPanel" id ="nameArea">
    
    </div>
    <div class="container chat">
        <div id="chatlog" class="row">
           <div class="scroll"  id="scrollId">
                    <?php echo(file_get_contents("chat.txt")) ?>
            </div>
        </div>
       
    </div>
    <div class="input-group">
        <textarea class="form-control" id="message" rows="3" maxlength='100'></textarea>
            <span class="input-group-btn">
              <button class="btn btn-default" id="send" type="button" title="send">Send</button>
            </span>
    </div>
</div>



           <script type="text/javascript">

        // ask user for name with popup prompt    
        
        var name = prompt("Enter your chat name:", "Guest");
        
        // default name is 'Guest'
    	if (!name || name === ' ' || name==="null") {
    	   name = "Guest";	
    	}
    	
    	// strip tags
    	name = name.replace(/(<([^>]+)>)/ig,"");
 
    	// display name on page
    	$("#nameArea").html('<div id="userName" class="row">Welcome, '+ name + " </div>");
    getStateOfChat();
    	
    	   $('#send').click(sendMessage);
    $( "#message" ).keydown(function( event ) {
      if (event.keyCode == 13 ) {
        sendMessage();
      } 
    });

   </script>
    

</body>
</html>